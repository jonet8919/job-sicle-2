@extends('layouts.app')
@section('content')
<div class="contents">
    <div class="profile-setting ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-main">
                        <h4 class="text-capitalize breadcrumb-title">My profile</h4>
                    </div>
                </div>
                @include('layouts.partials._menu_jobseeker')
                <div class="col-xxl-9 col-lg-8 col-sm-7">
                   
                    <div class="mb-50">
                        <div class="edit-profile mt-0">
                            <div class="card">
                                <div class="card-header px-sm-25 px-3">
                                    <div class="edit-profile__title">
                                        <h6>Introduce your self to employers</h6>
                                        <span class="fs-13 color-light fw-400">Let’s create your Jobsicle Resume before entering the job hunt.</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-xxl-6 col-lg-8 col-sm-10">
                                            <div class="edit-profile__body mx-lg-20">
                                                <form>
                                                    <input type="hidden" class="form-control" value="{{$basic_information->resume->id}}" id="id" name="id" disabled>
                                                    <div class="form-group mb-20">
                                                        <label for="introduction">Your Introduction</label>
                                                        <textarea class="form-control" id="introduction" rows="5" value="{{$basic_information->resume->introduction}}">{{$basic_information->resume->introduction}}</textarea>
                                                    </div>
                                                    <div class="button-group d-flex flex-wrap pt-30 mb-15">
                                                        <button type="button" class="btn btn-primary btn-default btn-squared mr-15 text-capitalize" onclick="updated()">Save Changes
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function updated(){
        var introduction = $('#introduction').val();
        var id = $('#id').val();
        createOverlay("process...");
        $.ajax({
            type : "POST",
            url: "/jobseeker/my-profile/introduction/update/"+id,
            data: {
                '_token' : '{{ csrf_token() }}',
                'introduction' :introduction,
            },
            success: function(data){
                gOverlay.hide();
                if(data["status"] == "success") {            
                    toastr.success(data["message"]);          
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });

    }
</script>

@endsection