@extends('layouts.app')
@section('content')
<div class="contents">
    <div class="profile-setting ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-main">
                        <h4 class="text-capitalize breadcrumb-title">My profile</h4>
                    </div>
                </div>
                @include('layouts.partials._menu_jobseeker')
                <div class="col-xxl-9 col-lg-8 col-sm-7">
                    <div class="mb-50">
                        <div class="edit-profile mt-0">
                            <div class="card">
                                <div class="card-header px-sm-25 px-3">
                                    <div class="edit-profile__title">
                                        <h6>Let's fill your basic information</h6>
                                        <span class="fs-13 color-light fw-400">Keep it updated on any changes.</span>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="row justify-content-center">
                                        <div class="col-xxl-6 col-lg-8 col-sm-10">
                                            <div class="edit-profile__body mx-lg-20">
                                                <form enctype="multipart/form-data">
                                                   
                                                    <div class="account-profile pt-25 px-25 pb-0 flex-column d-flex align-items-center ">
                                                        <div class="ap-img mb-20 pro_img_wrapper">
                                                            <input id="image_profile" type="file" name="image_profile" class="d-none">
                                                            <label for="image_profile">
                                                                <!-- Profile picture image-->
                                                                <img class="ap-img__main rounded-circle wh-120" src="{{ asset('image_profile/'.$basic_information->image_profile)}}" alt="profile">
                                                                <span class="cross" id="remove_pro_pic">
                                                                    <span data-feather="camera"></span>
                                                                </span>
                                                            </label>
                                                        </div>
                                                        
                                                    </div>
                                                    {{-- <div class="ap-img mb-20 pro_img_wrapper">
                                                        <input id="file-upload" type="file" name="fileUpload" class="d-none">
                                                        <label for="file-upload">
                                                            <img class="ap-img__main rounded-circle wh-120" src="{{ asset('image_profile/'.$basic_information->image_profile)}}" alt="profile">
                                                            <span class="cross" id="remove_pro_pic">
                                                                <span data-feather="camera"></span>
                                                            </span>
                                                        </label>
                                                    </div> --}}
                                                    <input type="hidden" class="form-control" value="{{$basic_information->id}}" id="id" name="id" disabled>
                                                    <div class="form-group mb-20">
                                                        <label for="name">Full Name</label>
                                                        <input type="text" class="form-control" id="name" id="name" value="{{$basic_information->name}}" placeholder="Duran Clayton">
                                                    </div>
                                                    <div class="form-group mb-20">
                                                        <label for="phoneNumber1">Mobile Number</label>
                                                        <div class="row">
                                                            <div class="col-md-5 mb-25">
                                                                @if(count($list_country_code) > 0)
                                                                <select class="js-example-basic-single js-states form-control" data-size="7" title="Code" name="country_code" id="country_code">
                                                                    @foreach($list_country_code as $id => $value)
                                                                        <option value="{{ $id }}" {{ $basic_information->country_code_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                                    @endforeach
                                                                </select>
                                                                
                                                                @else
                                                                    <p>There are no Nationality options, make it first ...!</p>
                                                                @endif
                                                            </div>
                                                            <div class="col-md-7 mb-25">
                                                                <input type="tel" class="form-control  ih-medium ip-lightradius-xs b-light" name="phone" id="phone" value="{{$basic_information->phone}}" placeholder="84xxxxx">
                                                            </div>
                                                        </div>
                                                    </div>
                                                 

                                                    <div class="form-group mb-20">
                                                        <div class="cityOption">
                                                            <label for="cityOption">
                                                                Gender
                                                            </label>
                                                            <select class="js-example-basic-single js-states form-control" data-size="7" title="Select One" name="gender" id="gender">
                                                                <option value="1" {{ $basic_information->gender== "1" ? 'selected' : '' }}>Male</option>
                                                                <option value="2" {{ $basic_information->gender == "2" ? 'selected' : '' }}>Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-20">
                                                        <div class="cityOption">
                                                            <label for="cityOption">
                                                                Nationality
                                                            </label>
                                                          
                                                            @if(count($list_nationalities) > 0)
                                                                <select class="js-example-basic-single js-states form-control" data-size="7" title="Select One" name="nationality" id="nationality">
                                                                    @foreach($list_nationalities as $id => $value)
                                                                        <option value="{{ $id }}" {{ $basic_information->experience_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <p>There are no Nationality options, make it first ...!</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-20">
                                                        <label for="dob">Date of Birth</label>
                                                        <input type="date" class="form-control" name="dob" id="dob" value="{{ \Carbon\Carbon::parse($basic_information->dob)->format('Y-m-d') }}">
                                                    </div>

                                                    <div class="form-group mb-20">
                                                        <div class="cityOption">
                                                            <label for="cityOption">
                                                                Employment status:
                                                            </label>
                                                            <select class="js-example-basic-single js-states form-control" data-size="7" title="Select One" name="employment_status" id="employment_status">
                                                                <option value="1" {{ $basic_information->employment_status== "1" ? 'selected' : '' }}>Currently in a job</option>
                                                                <option value="2" {{ $basic_information->employment_status == "2" ? 'selected' : '' }}>Not employed, looking for a job</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group mb-20">
                                                        <div class="cityOption">
                                                            <label for="cityOption">
                                                                Highest Qualification
                                                            </label>
                                                          
                                                            @if(count($list_qualification) > 0)
                                                                <select class="js-example-basic-single js-states form-control" data-size="7" title="Select One" name="qualification_id" id="qualification_id">
                                                                    @foreach($list_qualification as $id => $value)
                                                                        <option value="{{ $id }}" {{ $basic_information->qualification_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <p>There are no Highest Qualification, make it first ...!</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group mb-20">
                                                        <div class="cityOption">
                                                            <label for="cityOption">
                                                                Total Work Experience
                                                            </label>
                                                          
                                                            @if(count($list_experience) > 0)
                                                                <select class="js-example-basic-single js-states form-control" data-size="7" title="Select One" name="experience" id="experience">
                                                                    @foreach($list_experience as $id => $value)
                                                                        <option value="{{ $id }}" {{ $basic_information->experience_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <p>There are no Present location, make it first ...!</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group mb-20">
                                                        <div class="cityOption">
                                                            <label for="cityOption">
                                                                Present location (country/city)
                                                            </label>
                                                          
                                                            @if(count($list_location) > 0)
                                                                <select class="js-example-basic-single js-states form-control" data-size="7" title="Select One" name="location" id="location">
                                                                    @foreach($list_location as $id => $value)
                                                                        <option value="{{ $id }}" {{ $basic_information->location_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                                    @endforeach
                                                                </select>
                                                            @else
                                                                <p>There are no Present location, make it first ...!</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="button-group d-flex flex-wrap pt-30 mb-15">
                                                        <button type="button" class="btn btn-primary btn-default btn-squared mr-15 text-capitalize" onclick="updated()">update profile
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            
                </div>
            </div>
        </div>
    </div>
</div>

<script> 
    function updated(){
        var id = $('#id').val();
        var image_profile = $('#image_profile').prop('files')[0];
        var name = $('#name').val();
        var country_code = $('#country_code').val();
        var phone = $('#phone').val();
        var gender = $('#gender').val();
        var nationality = $('#nationality').val();
        var dob = $('#dob').val();
        var employment_status = $('#employment_status').val();
        var qualification_id = $('#qualification_id').val();
        var experience = $('#experience').val();
        var location = $('#location').val();

        var form_data = new FormData();
        form_data.append('_token', '{{ csrf_token() }}');
        form_data.append('id', id);
        form_data.append('name', name);
        form_data.append('country_code_id', country_code);
        form_data.append('phone',phone);
        form_data.append('gender', gender);
        form_data.append('nationality_id', nationality);
        form_data.append('dob', dob);
        form_data.append('employment_status', employment_status);
        form_data.append('qualification_id',qualification_id);
        form_data.append('experience_id', experience);
        form_data.append('location_id', location);
        form_data.append('image_profile', image_profile);

        createOverlay("process...");

        $.ajax({
            type : "POST",
            url: "/jobseeker/my-profile/basic-information/update/"+id,
            dataType: 'json', 
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('my-profile.basic-information') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }
</script>
@endsection