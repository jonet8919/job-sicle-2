@extends('layouts.app')
@section('content')
<div class="contents">

    <div class="profile-setting mb-20">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb-main">
                        <h4 class="text-capitalize breadcrumb-title">My profile</h4>
                    </div>
                </div>
                <div class="col-lg-12">
                    <!-- Profile Acoount -->
                    <div class="card">
                        <div class="card-header  px-sm-25 px-3">
                            <div class="edit-profile__title">
                                <h6>change password</h6>
                                <span class="fs-13 color-light fw-400">Change or reset your account
                                    password</span>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row justify-content-center">
                                <div class="col-xxl-6 col-lg-8 col-sm-10">
                                    <div class="edit-profile__body mx-lg-20">
                                        <form>
                                            <div class="form-group mb-20">
                                                <label for="password">old passowrd</label>
                                                <input type="text" class="form-control" id="password">
                                            </div>
                                            <div class="form-group mb-1">
                                                <label for="new-password">new password</label>
                                                <div class="position-relative">
                                                    <input id="new-password" type="password" class="form-control pr-50" name="new-password">
                                                    <span class="fa fa-fw fa-eye-slash text-light fs-16 field-icon toggle-password2"></span>
                                                </div>
                                                <small id="passwordHelpInline" class="text-light fs-13">Minimum
                                                    6
                                                    characters
                                                </small>
                                            </div>
                                            <div class="form-group mb-1">
                                                <label for="retype-password">Retype New Password</label>
                                                <div class="position-relative">
                                                    <input id="retype-password" type="password" class="form-control pr-50" name="retype-password" value="secret">
                                                    <span class="fa fa-fw fa-eye-slash text-light fs-16 field-icon toggle-password2"></span>
                                                </div>
                                                <small id="passwordHelpInline" class="text-light fs-13">Minimum
                                                    6
                                                    characters
                                                </small>
                                            </div>
                                            <div class="button-group d-flex flex-wrap pt-45 mb-35">
                                                <button type="button" id="update" onclick="updated()" class="btn btn-primary btn-default btn-squared mr-15 text-capitalize">Save Changes
                                                </button>
                                               
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Profile Acoount End -->
                </div>
            </div>
        </div>
    </div>
</div>
<script> 
    function updated(){ 
        var password = $('#password').val();
        var newpassword = $('#new-password').val();
        var retypepassword = $('#retype-password').val();

        createOverlay("process...");

        $.ajax({
            type : "POST",
            url: "{!! route('security.change-password-post') !!}",
            data: {
                '_token' : '{{ csrf_token() }}',
                'current_password' :password,
                'password' :newpassword,
                'password_confirmation' :retypepassword,
            },
            success: function(data){
                gOverlay.hide();

                if(data["status"] == "success") {            
                    toastr.success(data["message"]);
                    setTimeout(function(){ 
                        window.location = "{{ route('security.change-password') }}";
                    }, 500);            
                }else {
                    toastr.error(data["message"]);
                }
            },
            error: function(error) {
                alert("Server/network error\r\n" + error);
            }
        });
    }
</script>
@endsection