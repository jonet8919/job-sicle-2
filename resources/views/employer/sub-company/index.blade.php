@extends('companies.layouts.app')
@section('styles')
<style>
    .dialog-with-tabs .button {
        height: 48px;
        width: 100% !important;
        margin-top: -24px;
        box-shadow: 0 4px 12px rgb(102 103 107 / 15%);
    }
</style>
@endsection
@section('content')
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Sub Company</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Companies</a></li>
						<li>Sub Company</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
            <div class="row">
				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-account-circle"></i> Tell employers about your <b>Sub Company</b></h3>
                            {{-- <a href="{{route('sub-company.create')}}" title="Add Sub Company"> Add Sub Company</a> --}}
							<div class="right-side">
                                <!-- Breadcrumbs -->
                                <div id="breadcrumbs" class="btn btn-success">
                                    <ul>
                                        <a href="{{route('sub-company.create')}}" title="Add Sub Company"> Add Sub Company</a>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        
						<div class="content with-padding padding-bottom-4">
                            <div class="col">
                                <div class="row">
                                    <div class="col-xl-12 col-md-12">
                                        <table class="basic-table">
											<tr>
												<td data-label="Column 1">Photo</td>
												<td data-label="Column 2">Company</td>
												<td data-label="Column 3">Founded On</td>
												<td data-label="Column 4">Action</td>
											</tr>
											@foreach ($sub_company as $item)
												<tr>
													<td data-label="Column 1"><img class="profile-pic" @if($item->logo != NULL) src="{{asset('logo_company/'.$item->logo)}}" onerror="this.onerror=null;this.src='{{asset('images/user-avatar-placeholder.png')}}';" width="50px" height="50px"@endif alt="" /></td>
													<td data-label="Column 2">{{$item->name}}</td>
													<td data-label="Column 3">{{$item->founded_on}}</td>
													<td data-label="Column 4">
														<a href="{{route('company.detail', [$item->id])}}" class="button dark ripple-effect ico" title="View Company" data-tippy-placement="top"><i class="icon-feather-globe"></i></a>
														<a href="{{route('sub-company.edit', [$item->id])}}" class="button green ripple-effect ico" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" title="Edit Sub Company" data-tippy-placement="top"><i class="icon-feather-edit"></i></a>
														<form action="{{ route('sub-company.delete', [$item->id]) }}" method="post">
															{{ csrf_field() }}
															{{ method_field('DELETE') }}
															<button class="button red ripple-effect ico" class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm" type="submit" onclick="return confirm('Are you sure to DELETE this data?')"><i class="icon-feather-trash-2"></i></button>
														</form>
													</td>
												</tr>
											@endforeach
                            
                                        </table>
                                    </div>

                                </div>
                            </div>
						</div>

					
					</div>
				</div>

			</div>
			<!-- Row / End -->
			<!-- Footer -->
            @include('layouts.partials._footer_dashboard')
			<!-- Footer / End -->

		</div>
	</div>
	<!-- Dashboard Content / End -->
    @include('jobseekers.my-profiles.qualifications.modal')
	@include('jobseekers.my-profiles.qualifications.delete')

@endsection
