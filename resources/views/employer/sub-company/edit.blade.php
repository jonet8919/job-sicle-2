@extends('companies.layouts.app')
@section('content')
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Basic Company information</h3>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs" class="dark">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">My Profile</a></li>
						<li>Basic information</li>
					</ul>
				</nav>
			</div>
	
			<!-- Row -->
            <div class="row">

				<!-- Dashboard Box -->
				<div class="col-xl-12">
					<div class="dashboard-box margin-top-0">

						<!-- Headline -->
						<div class="headline">
							<h3><i class="icon-material-outline-account-circle"></i> Let's fill your <b>basic information</b></h3>
                            <div class="right-side">
                                <!-- Breadcrumbs -->
                                <div id="breadcrumbs" class="btn btn-success">
                                    <ul>
                                        <a href="{{route('company.detail', [$sub_company->id])}}" class="btn btn-success">Detail Company</a>
                                    </ul>
                                </div>
                            </div>
						</div>

						<div class="content with-padding padding-bottom-0">
                            <form enctype="multipart/form-data">
							<div class="row">

								<div class="col-auto">
									<div class="avatar-wrapper" data-tippy-placement="bottom" title="Change Avatar">
										<img class="profile-pic" src="{{asset('logo_company/'.$sub_company->logo)}}" onerror="this.onerror=null;this.src='{{asset('images/user-avatar-placeholder.png')}}';" alt="" />
										<div class="upload-button"></div>
										<input class="file-upload" type="file" accept="image/*" id="logos" name="logos"/>
									</div>
								</div>

								<div class="col">
									<div class="row">

                                        <input type="hidden" class="form-control" value="{{$sub_company->id}}" id="idCompany" name="idCompany" disabled>
										<div class="col-xl-12">
											<div class="submit-field">
												<h5>Full Name</h5>
												<input type="text" class="with-border" name="name" id="name" value="{{$sub_company->name}}">
											</div>
										</div>

                                        <div class="col-xl-12">
											<div class="submit-field">
												<h5>Email</h5>
												<input type="text" class="with-border" name="email" id="email" value="{{$sub_company->email}}">
											</div>
										</div>

                                        <div class="col-xl-6">
											<div class="submit-field">
                                                <h5>Sector</h5>
                                                @if(count($list_sector) > 0)
                                                    <select class="selectpicker with-border" data-size="7" title="Select One" name="sector_id" id="sector_id">
                                                        @foreach($list_sector as $id => $value)
                                                            <option value="{{ $id }}" {{ $sub_company->sector_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                    <p>There are no Sector options, make it first ...!</p>
                                                @endif
                                            </div>
										</div>

                                        <div class="col-xl-6">
                                            <div class="submit-field">
                                            <h5>Mobile Number</h5>
                                            <div class="row">
                                                <div class="col-xl-4">
                                                    @if(count($list_country_code) > 0)
                                                        <select class="selectpicker with-border" data-size="7" title="Code" name="country_code" id="country_code">
                                                            @foreach($list_country_code as $id => $value)
                                                                <option value="{{ $id }}" {{ $sub_company->country_code_id == $id ? 'selected' : '' }}>{{ $value }}</option>
                                                            @endforeach
                                                        </select>
                                                    @else
                                                        <p>There are no Nationality options, make it first ...!</p>
                                                    @endif
                                                </div>
                                                <div class="col-xl-8">
                                                    <div class="submit-field">
                                                        <input type="number" class="with-border" name="phone" id="phone" value="{{$sub_company->phone}}">
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
										</div>
                                        
                                        <div class="col-xl-12">
											<div class="submit-field">
												<h5>Website</h5>
												<input type="text" class="with-border" name="website" id="website" value="{{$sub_company->website}}">
											</div>
										</div>

                                        <div class="col-xl-6">
											<div class="submit-field">
												<h5>Founded On</h5>
												<input type="date" class="with-border" name="founded_on" id="founded_on" value="@if(isset($sub_company->founded_on)){{ \Carbon\Carbon::parse($sub_company->founded_on)->format('Y-m-d') }}@endif">
											</div>
										</div>

                                        <div class="col-xl-6">
											<div class="submit-field">
												<h5>Company Size</h5>
												<input type="text" class="with-border" name="company_size" id="company_size" value="{{$sub_company->company_size}}">
											</div>
										</div>

                                        <div class="col-xl-12">
											<div class="submit-field">
												<h5>Mission</h5>
												<input type="text" class="with-border" name="mission" id="mission" value="{{$sub_company->mission}}">
											</div>
										</div>
                                        
                                        <div class="col-xl-12">
											<div class="submit-field">
												<h5>Introduction</h5>
												<textarea type="text" class="with-border" name="introduction" id="introduction">{{$sub_company->introduction}}</textarea>
											</div>
										</div>

                                        <div class="col-xl-12">
											<div class="submit-field">
												<h5>Why Work with Us</h5>
												<textarea type="text" class="with-border" name="why_work_with_us" id="why_work_with_us">{{$sub_company->why_work_with_us}}</textarea>
											</div>
										</div>

									</div>
                                    {{-- <div class="col-xl-12">
                                        <div class="submit-field">
                                            <h5>Companies Gallery</h5>
                                            
                                            <div class="uploadButton margin-top-30">
                                                <input type="file" accept="image/*, application/pdf" id="gallery_company" name="gallery_company[]" multiple/>
                                            </div>
                                            <div class="uploadButton margin-top-30">
                                                <input type="file" accept="image/*, application/pdf" id="gallery_company" name="gallery_company[]" multiple/>
                                            </div>
                                            <div class="uploadButton margin-top-30">
                                                <input type="file" accept="image/*, application/pdf" id="gallery_company" name="gallery_company[]" multiple/>
                                            </div>
                                            <div class="uploadButton margin-top-30">
                                                <input type="file" accept="image/*, application/pdf" id="gallery_company" name="gallery_company[]" multiple/>
                                            </div>
                                        </div>
                                    </div> --}}

								</div>
							</div>
                        </form>
						</div>
					</div>
				</div>
                

				<!-- Button -->
				<div class="col-xl-12">
					<button class="button ripple-effect big margin-top-30" onclick="updated()">Save Changes</button>
				</div>

			</div>
			<!-- Row / End -->
			<!-- Footer -->
            @include('layouts.partials._footer_dashboard')
			<!-- Footer / End -->

		</div>
	</div>
	<!-- Dashboard Content / End -->

<script> 
        function updated(){
            var idCompany = $('#idCompany').val();
            var logos = $('#logos').prop('files')[0];
            var name = $('#name').val();
            var email = $('#email').val();
            var sector_id = $('#sector_id').val();
            var phone = $('#phone').val();
            var website = $('#website').val();
            var founded_on = $('#founded_on').val();
            var company_size = $('#company_size').val();
            var mission = $('#mission').val();
            var introduction = $('#introduction').val();
            var why_work_with_us = $('#why_work_with_us').val();

            var form_data = new FormData();
            form_data.append('_token', '{{ csrf_token() }}');
            form_data.append('logos', logos);
            form_data.append('id', idCompany);
            form_data.append('name', name);
            form_data.append('email', email);
            form_data.append('sector_id', sector_id);
            form_data.append('phone',phone);
            form_data.append('website', website);
            form_data.append('founded_on', founded_on);
            form_data.append('company_size', company_size);
            form_data.append('mission',mission);
            form_data.append('introduction', introduction);
            form_data.append('why_work_with_us', why_work_with_us);
            createOverlay("process...");

            $.ajax({
                type : "POST",
                url: "/company/company-profile/sub-company/"+idCompany,
                dataType: 'json', 
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                success: function(data){
                    gOverlay.hide();

                    if(data["status"] == "success") {            
                        toastr.success(data["message"]);
                        setTimeout(function(){ 
                            window.location = "{{ route('sub-company.index') }}";
                        }, 500);            
                    }else {
                        toastr.error(data["message"]);
                    }
                },
                error: function(error) {
                    alert("Server/network error\r\n" + error);
                }
            });
        }
</script>
@endsection
