<div class="header">
    <nav class="navbar fixed-top navbar-expand-lg px-md-4 mb-3 bg-custom">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}"><img class="w-50" src="{{asset('image/'.config('mail.from.app_logo'))}}" alt=""></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"><i class="im im-menu"></i></span>
          </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link px-md-3 active" aria-current="page" href="#">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-md-3" aria-current="page" href="#">Job</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-md-3" aria-current="page" href="#">Employers</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-md-3" aria-current="page" href="#">Talentpool</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-md-3" aria-current="page" href="#">About</a>
                    </li>
                </ul>
                <div class="d-flex px-md-3">
                    @if (Auth::check())
                    <div class="nav-item dropdown">
                        <button class="btn nav-link dropdown-toggle" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="im im-user-circle"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @if(!empty(Auth::user()->getRoleNames()))
									@foreach(Auth::user()->getRoleNames() as $key =>$v)
										@if ($v=="employer")
											<li><a class="dropdown-item d-flex justify-content-around bg-customize" href="{{route('company.dashboard')}}"><i class="im im-dashboard"></i> Dashboard</a></li>
										@elseif($v=="jobseeker")
											<li><a class="dropdown-item d-flex justify-content-around bg-customize" href="{{route('jobseeker.dashboard')}}"><i class="im im-dashboard"></i> Dashboard</a></li>
										@elseif($v=="admin")
											<li><a  class="dropdown-item d-flex justify-content-around bg-customize" href="{{route('admin.dashboard')}}"><i class="im im-dashboard"></i> Dashboard</a></li>
										@endif
									@endforeach
								@endif
            
                            <a class="dropdown-item d-flex justify-content-around bg-customize" href="{{ route('logout') }}"  onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="im im-sign-out"></i><p>Log out</p></a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </div>
                    @else
                        <a href="{{route('login')}}" class="btn btn-primary"> Login</a>
                    @endif
                    
                </div>
            </div>
        </div>
    </nav>
</div>