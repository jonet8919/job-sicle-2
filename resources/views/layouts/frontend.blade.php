<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }} | @yield('title')</title>
        @include('layouts.partials_frontend._styles')
    </head>
    <body>
        <div class="@yield('name_class')">
            <!-- Header -->
            @include('layouts.partials_frontend._header')
            <!-- Header-End -->

            <!--Form-Start-->
            @yield('content')
            <!--Form-End-->

            <!--Footer-Start-->
            @include('layouts.partials_frontend._footer')
        </div>
        <!--Footer-End-->
        @include('layouts.partials_frontend._scripts')
    </body>
</html>
