<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name: ', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('permission') ? 'has-error' : ''}}">
    {!! Form::label('permission', 'Permission', ['class' => 'control-label']) !!}
    @if(count($permissions) > 0)
        @foreach($permissions as $key => $value)
        <br>
            <label>{!! Form::checkbox('permission[]', $key,  isset($role->id) ?  in_array($key, $rolePermissions) ? true : null : null,array('class' => 'name')) !!} {{ $value }}</label>
        
            @endforeach
        {!! $errors->first('permission', '<p class="help-block">:message</p>') !!}
    @else
        <p>There are no permission options, make it first ...!</p>
    @endif
</div>
<div class="button-group d-flex pt-25 justify-content-end">

    {!! Form::submit($formMode === 'edit' ? 'Update' : 'Create', ['class' => 'btn btn-primary btn-default btn-squared text-capitalize radius-md shadow2 btn-sm']) !!}

 </div>

