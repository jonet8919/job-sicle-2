@extends('layouts.app')
@section('content')
<div class="contents">
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12">
            <div class="d-flex align-items-center user-member__title mb-30 mt-30">
               <h4 class="text-capitalize">add Role</h4>
            </div>
         </div>
      </div>
      <div class="card mb-50">
         <div class="row justify-content-center">
            <div class="col-sm-5 col-10">
               <div class="mt-40 mb-50">
                  <div class="edit-profile__body">
                  
                     @if ($errors->any())
                           <ul class="alert alert-danger">
                              @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                              @endforeach
                           </ul>
                     @endif

                 {!! Form::open(['url' => '/admin/roles', 'class' => 'form-horizontal']) !!}

                 @include ('backend.roles.form', ['formMode' => 'create'])

                 {!! Form::close() !!}
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection