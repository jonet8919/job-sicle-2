@extends('layouts.app')
@section('content')
<div class="contents">
   <div class="container-fluid">
      <div class="row">
         <div class="col-lg-12">
            <div class="contact-breadcrumb">
               <div class="breadcrumb-main add-contact justify-content-sm-between ">
                  <div class=" d-flex flex-wrap justify-content-center breadcrumb-main__wrapper">
                     <div class="d-flex align-items-center add-contact__title justify-content-center mr-sm-25">
                        <h4 class="text-capitalize fw-500 breadcrumb-title">List Salary Ranges</h4>
                        <span class="sub-title ml-sm-25 pl-sm-25"></span>
                     </div>
                     <div class="action-btn mt-sm-0 mt-15">
                        {{-- @can ('users-create')    --}}
                           <button type="button" class="btn btn-primary btn-sm"  onClick="create()" data-toggle="modal" data-target="#modal-basic">   <i class="las la-plus fs-16"></i>Add New</button>
                        {{-- @endcan --}}
                     </div>
                  </div>
                  <div class="breadcrumb-main__wrapper">
                     
                     <form action="{{ url('/admin/master/salary-ranges') }}" method="GET" class="d-flex align-items-center add-contact__form my-sm-0 my-2">
                        <span data-feather="search"></span>
                        <input class="form-control mr-sm-2 border-0 box-shadow-none" name="search" value="{{ request('search') }}" type="search" id="userName" onkeyup="userFunction()" placeholder="Search by Name" aria-label="Search">
                     </form>
                  </div>
               </div>
            </div>
            <!-- ends: contact-breadcrumb -->
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12 mb-30">
            <div class="card">
               <div class="card-header color-dark fw-500">
               View List
               </div>
               <div class="card-body">
                  <div class="userDatatable global-shadow border-0 bg-white w-100">
                  @include('layouts.partials._message')
                     <div class="table-responsive">
                        <table id="userTable" class="table mb-0 table-borderless">
                           <thead>
                              <tr class="userDatatable-header">
                                  <th>
                                     <span class="userDatatable-title">#</span>
                                  </th>
                                  <th>
                                     <span class="userDatatable-title">Name</span>
                                  </th>
                                
                                 <th>
                                    <span class="userDatatable-title float-right">action</span>
                                 </th>
                              </tr>
                           </thead>
                           <tbody>
                               @foreach ($collection as $item)
                              <tr>
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ $loop->iteration }}
                                    </div>
                                 </td>
                                 <td>
                                    <div class="userDatatable-content">
                                       {{ $item->name }}
                                    </div>
                                 </td>
                               
                       
                                 
                                 <td>
                                    <ul class="orderDatatable_actions mb-0 d-flex flex-wrap">
                                       {{-- @can ('users-edit')    --}}
                                          <li>
                                             <button type="button" onClick="edit({{ json_encode($item) }})" class="btn btn-icon btn-circle btn-outline-light btn-sm" data-toggle="modal" data-target="#modal-basic"> <span data-feather="edit"></span></button>
                                          </li>
                                       {{-- @endcan --}}
                                       {{-- @can ('users-delete') --}}
                                          <li>
                                             <button type="button" onClick="remove({{ $item->id}})" class="btn btn-icon btn-circle btn-outline-light btn-sm" data-toggle="modal" data-target="#modal-info-confirmed" > <span data-feather="trash-2"></span></button>
                                          </li>
                                       {{-- @endcan --}}
                                       <li>
                                       </li>
                                    </ul>
                                 </td> 
                              </tr>
                              @endforeach
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@include('backend.master-salary-range.modal')
@include('backend.master-salary-range.delete')
@endsection
