<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterLanguage;
class MasterLanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterLanguage::create([
            'name' => 'indo',
        ]);
    }
}
