<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User; 
use App\Models\JobSeekerBasicInfo;
use App\Models\JobSeekerSkills;
use App\Models\JobSeekerPreferences;
use App\Models\Companies;
use App\Models\WorkPhotos;
use App\Models\OfficeHours;
use App\Models\Resume;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name'=>'Admin',
            'email'=>'admin@gmail.com',
            'password'=> bcrypt('secret'),
        ]);

        $employer = User::create([
            'name'=>'Employer User',
            'email'=>'employer@gmail.com',
            'password'=> bcrypt('secret'),
        ]);

        $basic_company_information = Companies::create([
            'user_id' => $employer->id,
        ]);
        
        WorkPhotos::create([
            'company_id' => $basic_company_information->id,
        ]);
        
        OfficeHours::create([
            'company_id' => $basic_company_information->id,
        ]);

        $jobseeker = User::create([
            'name'=>'Job Seeker User',
            'email'=>'jobseeker@gmail.com',
            'password'=> bcrypt('secret'),
        ]);

        $basic_information = JobSeekerBasicInfo::create([
            'user_id' => $jobseeker->id,
        ]);
        
        $skill = [
            ['job_seeker_basic_info_id' => $basic_information->id, 'title' => 'Communication'],
            ['job_seeker_basic_info_id' => $basic_information->id, 'title' => 'Problem-solving'],
            ['job_seeker_basic_info_id' => $basic_information->id, 'title' => 'Time Management'],
            ['job_seeker_basic_info_id' => $basic_information->id, 'title' => 'Ability to Work Under Pressure'],
            ['job_seeker_basic_info_id' => $basic_information->id, 'title' => 'Computer Skills'],
        ];
        foreach ($skill as $item) {
            JobSeekerSkills::create($item);
        }
        JobSeekerPreferences::create([
            'user_id' => $jobseeker->id,
        ]);

        Resume::create([
            'job_seeker_basic_info_id' => $basic_information->id,
            'introduction'=>"test"
        ]);

        $admin->assignRole('admin');
        $employer->assignRole('employer');
        $jobseeker->assignRole('jobseeker');       
    }
}
