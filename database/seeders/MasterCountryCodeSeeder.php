<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterCountryCode;
class MasterCountryCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterCountryCode::create([
            'name' => '123',
        ]);
    }
}
