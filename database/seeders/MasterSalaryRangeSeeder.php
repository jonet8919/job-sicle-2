<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterSalaryRange;

class MasterSalaryRangeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSalaryRange::create([
            'name' => '3',
        ]);
    }
}
