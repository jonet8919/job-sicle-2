<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterSector;
class MasterSectorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterSector::create([
            'name' => '4',
        ]);
    }
}
