<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterLocation;
class MasterLocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterLocation::create([
            'name' => '1',
        ]);
    }
}
