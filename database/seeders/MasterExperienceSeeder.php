<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterExperience;
class MasterExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterExperience::create([
            'name' => 'ex',
        ]);
    }
}
