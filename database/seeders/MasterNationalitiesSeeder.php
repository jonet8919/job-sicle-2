<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterNationalities;
class MasterNationalitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MasterNationalities::create([
            'name' => '1',
        ]);
    }
}
