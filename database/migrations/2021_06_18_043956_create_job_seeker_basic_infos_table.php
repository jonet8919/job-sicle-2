<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobSeekerBasicInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_seeker_basic_infos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('nationality_id')->nullable();
            $table->foreignId('experience_id')->nullable();
            $table->foreignId('location_id')->nullable();
            $table->foreignId('qualification_id')->nullable();
            $table->foreignId('country_code_id')->nullable();
            $table->string('name')->nullable();
            $table->string('phone')->nullable();
            $table->date('dob')->nullable();
            $table->string('gender')->nullable()->comment('1 - Male, 2 - female');
            $table->string('employment_status')->nullable()->comment('1 - Currently in a job, 2 - Not employed, looking for a job');
            $table->string('image_profile')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_seeker_basic_infos');
    }
}
