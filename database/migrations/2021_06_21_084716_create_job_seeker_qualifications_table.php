<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobSeekerQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_seeker_qualifications', function (Blueprint $table) {
            $table->id();
            $table->foreignId('level_id')->nullable();
            $table->foreignId('resume_id')->nullable();
            $table->string('institute')->nullable();
            $table->string('year')->nullable();
            $table->string('course_name')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_seeker_qualifications');
    }
}
