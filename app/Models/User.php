<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function basicInformation(){
        return $this->hasOne(JobSeekerBasicInfo::class);
    }

    public function isAdmin()
    {
        foreach($this->roles()->get() as $role)
        {
            if ($role->name == 'admin')
            {
                return true;
            }
        }
    }

    public function isEmployer()
    {
        foreach($this->roles()->get() as $role)
        {
            if ($role->name == 'employer')
            {
                return true;
            }
        }
    }

    public function isJobSeeker()
    {
        foreach($this->roles()->get() as $role)
        {
            if ($role->name == 'jobseeker')
            {
                return true;
            }
        }
    }
}
