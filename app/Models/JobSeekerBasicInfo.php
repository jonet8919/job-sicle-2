<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobSeekerBasicInfo extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function resume(){
        return $this->hasOne(Resume::class);
    }
}
