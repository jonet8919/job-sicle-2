<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Companies;
use App\Models\WorkPhotos;
use App\Models\OfficeHours;
use Auth;
use File;
use Storage;
use Image;
use Hash;
use Validator;

class CompaniesController extends Controller
{
    public function basic_company_information_view()
    {
        $basic_company_information = Companies::where('user_id',Auth::user()->id)->first();
        return view('companies.basic-information',compact('basic_company_information'));
    }

    public function basic_company_information_update(Request $request, $id)
    {
        dd($request->all());
        $basic_company_information = Companies::where('id', $id)->first();
        // dd($request->hasFile('logos'));
        if ($request->hasFile('logos')) 
        {
            $rules =array(
                'logos' => 'nullable|file|image|max:2048',
            );
            $validator=Validator::make($request->all(),$rules);
            
            if($validator->fails())
            {
                $messages=$validator->messages();
                $errors=$messages->all();
                return response()->json(["status"=>"error","message"=>$errors[0]], 200);
            }

            $image = $request->file('logos');
            
            //name file
            $imagename = $image->getClientOriginalName();
            $a = explode(".", $imagename);
            $fileExt = strtolower(end($a));  
            $namaFile = substr(md5(date("YmdHis")),0,10).".".$fileExt;
            
            //penyimpanan
            $destination_path= public_path().'/logo_company/';
                
            // simpan ke folder
            $request->file('logos')->move($destination_path,$namaFile);

            // simpan nama ke database
            $basic_company_information->logo = $namaFile;
        }

        $basic_company_information->name = $request->name;
        $basic_company_information->email = $request->email;
        $basic_company_information->sector_id = $request->sector_id;
        $basic_company_information->phone = $request->phone;
        $basic_company_information->website = $request->website;
        $basic_company_information->founded_on = $request->founded_on;
        $basic_company_information->company_size = $request->company_size;
        $basic_company_information->mission = $request->mission;
        $basic_company_information->introduction = $request->introduction;
        $basic_company_information->why_work_with_us = $request->why_work_with_us;
        $basic_company_information->update();

        return response()->json(["status"=>"success","message"=>'Company Profile Updated!'], 200);
    }

    public function detail_company($id)
    {
        $basic_company_information = Companies::where('id', $id)->first();
        return view('companies.detail-company',compact('basic_company_information'));
    }

    public function index_sub_company()
    {
        $basic_company_information = Companies::where('user_id',Auth::user()->id)->first();
        $sub_company = Companies::where('parent_company_id', $basic_company_information->id)->get();
        return view('companies.sub-company.index',compact('basic_company_information', 'sub_company'));
    }

    public function create_sub_company()
    {
        $basic_company_information = Companies::where('user_id',Auth::user()->id)->first();
        $sub_company = Companies::where('parent_company_id', $basic_company_information->id)->get();
        return view('companies.sub-company.create',compact('basic_company_information', 'sub_company'));
    }

    public function store_sub_company(Request $request)
    {
        // dd($request->all());
        $parent = Companies::where('user_id',Auth::user()->id)->whereNull('parent_company_id')->first();
        $basic_company_information = new Companies();

        if ($request->hasFile('logos')) 
        {
            $rules =array(
                'logos' => 'nullable|file|image|max:2048',
            );
            $validator=Validator::make($request->all(),$rules);
            
            if($validator->fails())
            {
                $messages=$validator->messages();
                $errors=$messages->all();
                return response()->json(["status"=>"error","message"=>$errors[0]], 200);
            }

            $image = $request->file('logos');
            
            //name file
            $imagename = $image->getClientOriginalName();
            $a = explode(".", $imagename);
            $fileExt = strtolower(end($a));  
            $namaFile = substr(md5(date("YmdHis")),0,10).".".$fileExt;
            
            //penyimpanan
            $destination_path= public_path().'/logo_company/';
                
            // simpan ke folder
            $request->file('logos')->move($destination_path,$namaFile);

            // simpan nama ke database
            $basic_company_information->logo = $namaFile;
        }
        
        $basic_company_information->name = $request->name;
        $basic_company_information->user_id = $parent->user_id;
        $basic_company_information->parent_company_id = $parent->id;
        $basic_company_information->email = $request->email;
        $basic_company_information->sector_id = $request->sector_id;
        $basic_company_information->phone = $request->phone;
        $basic_company_information->website = $request->website;
        $basic_company_information->founded_on = $request->founded_on;
        $basic_company_information->company_size = $request->company_size;
        $basic_company_information->mission = $request->mission;
        $basic_company_information->introduction = $request->introduction;
        $basic_company_information->why_work_with_us = $request->why_work_with_us;
        $basic_company_information->save();

        return response()->json(["status"=>"success","message"=>'Sub Company Added!'], 200);
    }

    public function edit_sub_company($id)
    {
        $sub_company = Companies::where('id', $id)->first();
        return view('companies.sub-company.edit',compact('sub_company'));
    }

    public function update_sub_company(Request $request, $id)
    {
        // dd($request->all());
        $sub_company = Companies::where('id', $id)->first();
        // dd($request->hasFile('logos'));
        if ($request->hasFile('logos')) 
        {
            $rules =array(
                'logos' => 'nullable|file|image|max:2048',
            );
            $validator=Validator::make($request->all(),$rules);
            
            if($validator->fails())
            {
                $messages=$validator->messages();
                $errors=$messages->all();
                return response()->json(["status"=>"error","message"=>$errors[0]], 200);
            }

            $image = $request->file('logos');
            
            //name file
            $imagename = $image->getClientOriginalName();
            $a = explode(".", $imagename);
            $fileExt = strtolower(end($a));  
            $namaFile = substr(md5(date("YmdHis")),0,10).".".$fileExt;
            
            //penyimpanan
            $destination_path= public_path().'/logo_company/';
                
            // simpan ke folder
            $request->file('logos')->move($destination_path,$namaFile);

            // simpan nama ke database
            $sub_company->logo = $namaFile;
        }

        $sub_company->name = $request->name;
        $sub_company->email = $request->email;
        $sub_company->sector_id = $request->sector_id;
        $sub_company->phone = $request->phone;
        $sub_company->website = $request->website;
        $sub_company->founded_on = $request->founded_on;
        $sub_company->company_size = $request->company_size;
        $sub_company->mission = $request->mission;
        $sub_company->introduction = $request->introduction;
        $sub_company->why_work_with_us = $request->why_work_with_us;
        $sub_company->update();

        return response()->json(["status"=>"success","message"=>'Sub Company Updated!'], 200);
    }

    public function delete_sub_company($id)
    {
        // dd($request->all());
        Companies::where('id', $id)->delete();

        return redirect()->back();
    }
}
