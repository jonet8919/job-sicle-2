<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterLevel;
use Validator;

class MasterLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Level Table";
        $table_information = "Master Level Table";
        $name_tag = "Level";
        $perPage = 1;

        if (!empty($keyword)) {
            $collection = MasterLevel::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterLevel::paginate($perPage);
        }

        return view('backend.master-level.index', compact('collection','name_tag','table_name','table_information'));
    }

    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_levels',
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        
        $data['name'] = $request->name;

        $collection = MasterLevel::create($data);

        return response()->json(["status"=>"success","message"=>'Level added!'], 200);
    }

    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_levels,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterLevel::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Level updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterLevel::destroy($id);
        return response()->json(["status"=>"success","message"=>'Level deleted!'], 200);
    }
}
