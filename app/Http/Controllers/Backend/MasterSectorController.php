<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterSector;
use Illuminate\Support\Facades\Validator;

class MasterSectorController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Sector Table";
        $table_information = "Master Sector Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterSector::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterSector::paginate($perPage);
        }

        return view('backend.master-sector.index', compact('collection','table_name','table_information'));
    }

    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_sectors,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterSector::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Sector added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_sectors,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterSector::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Sector updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterSector::destroy($id);
        return response()->json(["status"=>"success","message"=>'Sector deleted!'], 200);
    }
    
}
