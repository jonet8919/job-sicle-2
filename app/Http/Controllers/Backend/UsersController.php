<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Carbon\Carbon;

use DB,Excel,PDF,Auth;
use Validator;
class UsersController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:users-list', ['only' => ['index']]);
        $this->middleware('permission:users-view', ['only' => ['show']]);
        $this->middleware('permission:users-create', ['only' => ['create','store']]);
        $this->middleware('permission:users-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:users-delete', ['only' => ['destroy']]);
        
    }


    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "User Table";
        $table_information = "User Table";
        $perPage = 15;

        if (!empty($keyword)) {
            $users = User::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $users = User::latest()->paginate($perPage);
        }

        return view('backend.users.index', compact('users','table_name','table_information'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        $roles = Role::select('id', 'name')->get();
        $roles = $roles->pluck('name', 'id');

        return view('backend.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rules =array(
            'name' => 'required',
            'email' => 'required|string|max:255|email|unique:users',
            'password' => 'required',
            'roles' => 'required',
        
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $data['email_verified_at'] = now();

        $user = User::create($data);

        foreach ($request->roles as $role) {
            $user->assignRole($role);
        }

        return response()->json(["status"=>"success","message"=>'User added!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('backend.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function edit($id)
    {
    
        $user = User::with('roles')->select('id', 'name', 'email')->findOrFail($id);
        $roles = Role::pluck('name','id')->all();
        $user_roles = $user->roles->pluck('id','id')->all();
    
        
        return view('backend.users.edit', compact('user', 'roles', 'user_roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int      $id
     *
     * @return void
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $this->validate(
            $request,
            [
                'name' => 'required',
                'roles' => 'required',
            ]
        );
        
        $data = $request->except('password');
        if ($request->has('password')) {
            $data['password'] = bcrypt($request->password);
        }

        $user = User::findOrFail($id);
        $user->update($data);
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($request->input('roles'));

        return response()->json(["status"=>"success","message"=>'User deleted!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        // dd($id);
        User::destroy($id);
        return response()->json(["status"=>"success","message"=>'User deleted!'], 200);
    }

    public function updatePassword(Request $request)
    {
    
        $rules =array(
            'password'              => 'required|min:8|same:password_confirmation',
            'password_confirmation' => 'required',
        );
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        
        $user = User::find($request->id);

        $user->password = bcrypt(request('password'));
        $user->save();
        return response()->json(["status"=>"success","message"=>'Password has been updated!'], 200);
    
    }
}
