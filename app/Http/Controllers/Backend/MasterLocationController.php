<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterLocation;
use Illuminate\Support\Facades\Validator;
class MasterLocationController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Location Table";
        $table_information = "Master Location Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterLocation::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterLocation::paginate($perPage);
        }

        return view('backend.master-location.index', compact('collection','table_name','table_information'));
    }
    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_locations,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterLocation::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Location added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_locations,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterLocation::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Location updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterLocation::destroy($id);
        return response()->json(["status"=>"success","message"=>'Location deleted!'], 200);
    }
    
}
