<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterSalaryRange;
use Illuminate\Support\Facades\Validator;

class MasterSalaryRangeController extends Controller
{
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $table_name = "Salary Range Table";
        $table_information = "Master Salary Range Table";
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterSalaryRange::where('name', 'LIKE', "%$keyword%")->paginate($perPage);
        } else {
            $collection = MasterSalaryRange::paginate($perPage);
        }

        return view('backend.master-salary-range.index', compact('collection','table_name','table_information'));
    }

    
    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_salary_ranges,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterSalaryRange::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Salary Range added!'], 200);
    
    }
    public function update(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_salary_ranges,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }
        $requestData = $request->all();
        $collection = MasterSalaryRange::findOrFail($requestData['id']);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Salary Range updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterSalaryRange::destroy($id);
        return response()->json(["status"=>"success","message"=>'Salary Range deleted!'], 200);
    }
    
}
