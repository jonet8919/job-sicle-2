<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\MasterCategory;
use Illuminate\Support\Facades\Validator;

class MasterCategoryController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $collection = MasterCategory::where('name', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $collection = MasterCategory::paginate($perPage);
        }
        
        return view('backend.master-category.index', compact('collection'));
    }

    public function store(Request $request)
    {
        $rules =array(
            'name' => 'required|unique:master_categories,name',
        );
        $validator=Validator::make($request->all(),$rules);
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        MasterCategory::create([
            'name' => $request->name,
        ]);
        return response()->json(["status"=>"success","message"=>'Category added!'], 200);
    
    }
    public function update(Request $request)
    {
       
        $rules =array(
            'name' => 'required|unique:master_categories,name,' .$request->name,
        );
        
        $validator=Validator::make($request->all(),$rules);
        
        if($validator->fails())
        {
            $messages=$validator->messages();
            $errors=$messages->all();
            return response()->json(["status"=>"error","message"=>$errors[0]], 200);
        }

        $requestData = $request->all();
        $collection = MasterCategory::findOrFail($request->id);
        $collection->update($requestData);
        return response()->json(["status"=>"success","message"=>'Category updated!'], 200);
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return void
     */
    public function destroy($id)
    {
        MasterCategory::destroy($id);
        return response()->json(["status"=>"success","message"=>'Category deleted!'], 200);
    }
    


}
